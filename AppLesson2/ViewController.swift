//
//  ViewController.swift
//  AppLesson2
//
//  Created by Глеб Зобнин on 21.09.2023.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var greetingLabel: UILabel!
    @IBOutlet weak var greetingButton: UIButton!
    //работа с оутлетами - в  IBAction и ViewDodLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        greetingLabel.isHidden = true
        greetingButton.layer.cornerRadius = 10
        greetingButton.setTitle("Show greeting", for: .normal)
    }
    
    @IBAction func greetingButtonPressed() {
        greetingLabel.isHidden.toggle() //переключает булево значение
        
        greetingButton.setTitle(
            greetingLabel.isHidden
            ? "Show greeting"
            : "Hide greeting",
            for: .normal
        )
    }
    

}

